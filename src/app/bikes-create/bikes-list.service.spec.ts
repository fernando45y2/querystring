import { TestBed } from '@angular/core/testing';

import { BikesListService } from './bikes-list.service';

describe('BikesListService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BikesListService = TestBed.get(BikesListService);
    expect(service).toBeTruthy();
  });
});
