import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: 'bikes',
    loadChildren: () => import('./components/bikes/bikes.module')
    .then(modulo => modulo.BikesModule)
  },
  {
    path: 'clients',
    loadChildren: () => import('./../app/clients/clients.module')
    .then(modulo => modulo.ClientsModule)
  },
  {
    path: 'ciclas',
    loadChildren: () => import('./../app/ciclas/ciclas.module')
    .then(modulo => modulo.CiclasModule)
  },
  {
    path: 'ventas',
    loadChildren: () => import('./../app/ventas/ventas.module')
    .then(modulo => modulo.VentasModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
