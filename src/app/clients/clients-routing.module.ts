import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClientsListComponent } from './clients-list/clients-list.component';
import { ClientsFormsComponent } from './clients-forms/clients-forms.component';


const routes: Routes = [
  {
    path: '',
    component: ClientsListComponent
  },
  {
    path: 'clients-list',
    component: ClientsListComponent
  },
  {
    path: 'clients-forms',
    component: ClientsFormsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientsRoutingModule { }
