import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ClientsService } from '../clients.service';

@Component({
  selector: 'app-clients-forms',
  templateUrl: './clients-forms.component.html',
  styleUrls: ['./clients-forms.component.styl']
})
export class ClientsFormsComponent implements OnInit {

  formu: FormGroup

  constructor(private _builder:FormBuilder,private clientsService: ClientsService) { 

    this.formu = this._builder.group({
      

    })
  }

  ngOnInit() {
  }
   
 

}
