import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CiclasListComponent } from './ciclas-list.component';

describe('CiclasListComponent', () => {
  let component: CiclasListComponent;
  let fixture: ComponentFixture<CiclasListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CiclasListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CiclasListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
