import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CiclasRoutingModule } from './ciclas-routing.module';
import { CiclasListComponent } from './ciclas-list/ciclas-list.component';


@NgModule({
  declarations: [CiclasListComponent],
  imports: [
    CommonModule,
    CiclasRoutingModule
  ]
})
export class CiclasModule { }
