import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CiclasListComponent } from './ciclas-list/ciclas-list.component';
import { ClientsListComponent } from '../clients/clients-list/clients-list.component';


const routes: Routes = [
  {
    path: '',
    component:CiclasListComponent
  },
  {
    path: 'ciclas-list',
    component:CiclasListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CiclasRoutingModule { }
