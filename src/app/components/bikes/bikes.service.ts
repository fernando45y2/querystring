import { Injectable } from '@angular/core';
import { IBike } from './interfaces/bike';
import { HttpClient, HttpParams} from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { $ } from 'protractor';


@Injectable({
  providedIn: 'root'
})
export class BikesService {

  constructor( private http: HttpClient ) { }

  public query(): Observable<IBike[]>  {
    return this.http.get<IBike[]>(`${environment.ENN_POINT}/comments`)
    .pipe(map(res => {
      return res;
    }));
  }
  /**
   * this metod save the bike
   * @param bike 
   */
  public saveBike(bike: IBike): Observable<IBike>{
    return this.http.post<IBike>(`${environment.ENN_POINT}/api/bikes`,bike)
    .pipe(map(res => {
      return res;
    }));
    
  }
  /**
   * metod for bring the object jeison from bikeservice
   * @param id 
   */
  public getBikeById(id: String): Observable<IBike>{
    return this.http.get<IBike>(`${environment.ENN_POINT}/api/bikes/${id}`)
    .pipe(map(res =>{
      return res;
    }));
  }
  public updateBike(id: String): Observable<IBike>{
    return this.http.get<IBike>(`${environment.ENN_POINT}/api/bikes/${id}`)
    .pipe(map(res =>{
      return res;
    }));
  }
  public getBikeBySerial(serial: string): Observable<IBike>{
    let params = new HttpParams();
    params.set('serial',serial);
    return this.http.get<IBike>(`${environment.ENN_POINT}/comments`)
    .pipe(map(res =>{
      return res;
    }));

  }

  
}
