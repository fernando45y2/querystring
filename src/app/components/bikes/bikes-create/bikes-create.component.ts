import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BikesService } from '../bikes.service';


@Component({
  selector: 'app-bikes-create',
  templateUrl: './bikes-create.component.html',
  styleUrls: ['./bikes-create.component.styl']
})
export class BikesCreateComponent implements OnInit {

  signupForm: FormGroup

  formSeaarchBike = this._builder.group({
    serial:['']
  });

  constructor(private _builder: FormBuilder,private bikeService: BikesService ) {


 this.signupForm = this._builder.group({
model:['',Validators.compose([Validators.maxLength(3), Validators.required])],
price:['',Validators.required],
serial:['',Validators.required]

 })
   }
   enviar(values){
    console.log(values)
  }
  saveBike(){
    console.log('Dato', this.signupForm.value);
    this.bikeService.saveBike(this.signupForm.value)
    .subscribe(res=>{
 console.log('guardado',res);

    }, error =>{
      console.error('error',error);

    });
    
  }

searchBike(){
  console.warn('data', this.formSeaarchBike.value);
  this.bikeService.getBikeBySerial(this.formSeaarchBike.value.serial)
  .subscribe(res =>{
    console.warn('respuesta por serial', res);
  },error =>{
    console.warn('no se Encuentra', error);
  })
}



  ngOnInit() {
  }

}

