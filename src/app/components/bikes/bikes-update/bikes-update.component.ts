import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BikesService } from '../bikes.service';
import { ActivatedRoute, Router } from '@angular/router';
import { IBike } from '../interfaces/bike';


@Component({
  selector: 'app-bikes-update',
  templateUrl: './bikes-update.component.html',
  styleUrls: ['./bikes-update.component.styl']
})
export class BikesUpdateComponent implements OnInit {
  signupForm: FormGroup

  constructor(private _builder:FormBuilder,
     private bikesService: BikesService,
     private activeteRoute: ActivatedRoute,
     private router: Router) {
    this.signupForm = this._builder.group({

      model:['',Validators.compose([Validators.maxLength(3), Validators.required])],
      price:['',Validators.required],
      serial:['',Validators.required]
      
      
    });
  }

  ngOnInit() {
    let id = this.activeteRoute.snapshot.paramMap.get('id');
    console.log('ID PATH', id);
    this.bikesService.getBikeById(id)
    .subscribe(res =>{
      console.log('get data ok', res);
      this.loadForm(res);
    });
  }
  saveBike(){
    console.log('Datos', this.signupForm.value);
    this.bikesService.updateBike(this.signupForm.value)
    .subscribe(res =>{
      console.log('Update ok', res);
      this.router.navigate(['/bikes/bikes-list'])
    }, error =>{
      console.error('Error', error);
    });
  }
  private loadForm(bike: IBike){
    this.signupForm.patchValue({
      postId:bike.postId,
      id:bike.id,
      name:bike.name,
      email:bike.email,
      body: bike.body
    });
  }

}
