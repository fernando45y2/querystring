import { Component, OnInit } from '@angular/core';
import { IBike } from '../interfaces/bike';
import { BikesService } from '../bikes.service';

@Component({
  selector: 'app-bikes-list',
  templateUrl: './bikes-list.component.html',
  styleUrls: ['./bikes-list.component.styl']
})
export class BikesListComponent implements OnInit {
  
public bikesList: IBike[];

  constructor(private bikesServer: BikesService) { }

  ngOnInit() {
    this.bikesServer.query()
    .subscribe(res => {
      this.bikesList = res;
      console.log('response data', res);
    },
    error => console.error('error',error)
    );
  }
}